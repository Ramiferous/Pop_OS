#!/bin/sh

# Check if the selected entry exists
get_entry () {
    NAME=$(rbw ls | fzf)

    if [ $? -eq 1 ]; then
        printf '%s\n' "Please, select a valid entry."
        exit 1
    elif [ -z "$NAME" ]; then
        printf '%s\n' "No entry selected, quitting."
	exit 0
    else
        get_password
    fi
}

get_password () {
    printf '%s\n' "Your password for \"$NAME\" has been copied to the clipboard"
    rbw get "$NAME" --raw | jq --join-output ".data.password" | xclip -loop 1 -sel clip >/dev/null
}

get_notes () {
    NOTE=$(rbw get --field notes "$NAME")
    if [ -n "$NOTE" ]; then
        printf '%s\n\n' "\"$NAME\" notes:"
	printf '%s\n\n' "$NOTE"
	printf '%s\n' "Press enter to exit."
	read ans
	exit 0
    fi
}

# Check if fzf, rbw and tmux exists in $PATH
for binary in rbw fzf; do
    command -v $binary 2>/dev/null 1>&2 || \
        { printf '%s\n' "$binary not found in \$PATH, make sure to install it before running this script" ; exit 1 ;}
done

# Unlock the vault before running the script
if rbw unlocked 2>/dev/null 1>&2; then
    get_entry
else
    rbw unlock && get_entry
fi

