#!/bin/sh

OUTPUT=/tmp/wallpaper/pscircle.png
BG=/home/dave/Pictures/walls/tilewall.png

mkdir -p /tmp/wallpaper

while true
do
	pscircle --background-image=$BG --output=$OUTPUT
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitoreDP1/workspace0/last-image -s $OUTPUT
    sleep 10
done
