#!/bin/bash

# ---------------------------------------------------------------
# Name: 	setbg
# Author: 	Cyr4x3
# Orig. Auth.: 	Luke Smith
# Descr.:	This script does the following:
#		Run by itself, set the wallpaper (at X start).
#		If given a file, set that as the new wallpaper.
#		If given a directory, choose random file in it.
#		If pywal is installed, also generates a colorscheme.
# Mod.:		If a single image is passed as an argument, you
#		can choose how to set wallpaper via dmenu.
#		If a folder is selected, 'zoom' is how the
# 		wallpaper is set.
# ---------------------------------------------------------------


# Location of link to wallpaper link.
bgloc="${XDG_DATA_HOME:-$HOME/.local/share/}/bg"

declare options=("center
stretch
tile
zoom
cancel")

trueloc="$(readlink -f "$1")" &&
case "$(file --mime-type -b "$trueloc")" in
	image/* ) ln -sf "$(readlink -f "$1")" "$bgloc"
		type=$(echo -e "${options[@]}" | dmenu -l 18 -nb '#000000' -nf '#695F5A' -sb '#B5B19E' -sf '#000000' -fn 'SarasaTermCLNerdFont-12' -l -i -p 'Set wallpaper: ')
		case "$type" in
			cancel)
				echo "Program terminated." && exit 1
			;;
			center)
				type="--center"
			;;
			stretch)
				type="--stretch"
			;;
			tile)
				type="--tile"
			;;
			zoom)
				type="--zoom"
			;;
			*)
				exit 1
			;;
		esac
		notify-send -i "Changing wallpaper..."
	;;
	inode/directory ) ln -sf "$(find "$trueloc" -iregex '.*.\(jpg\|jpeg\|png\|gif\)' -type f | shuf -n 1)" "$bgloc" && notify-send -i "$bgloc" "Random Wallpaper chosen." && type="--zoom";;
	*) notify-send "Error" "Not a valid image." ; exit 1;;
esac

# If pywal is installed, use it.
# command -v wal >/dev/null 2>&1 &&
	# wal -i "$trueloc" -o "${XDG_CONFIG_HOME:-$HOME/.config}/wal/postrun" >/dev/null 2>&1 &&
	# pidof dwm >/dev/null && xdotool key super+F12

xwallpaper "$type" "$bgloc"
