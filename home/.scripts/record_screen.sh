#! /bin/sh

#if [[ $# -ne 1 ]]; then
#    echo "video_single_window <base_file_name>" > /dev/stderr
#    exit 1
#fi

t=`mktemp`
xwininfo > $t
width=`grep Width $t | cut -d ":" -f 2 | tr -d " "`
height=`grep Height $t | cut -d ":" -f 2 | tr -d " "`
top_x=`grep "Absolute upper-left X" $t | cut -d ":" -f 2 | tr -d " "`
top_y=`grep "Absolute upper-left Y" $t | cut -d ":" -f 2 | tr -d " "`

ffmpeg \
  -f x11grab \
  -framerate 30 \
  -video_size ${width}x${height} \
  -i +${top_x},${top_y} \
  -c:v libx264rgb \
  -preset ultrafast \
  -crf 0 \
  $1.nut &
ffmpeg_pid=$!
hk Shift+F4 kill -SIGINT $ffmpeg_pid
wait $ffmpeg_pid

ffmpeg \
  -i $1.nut \
  -c:v libx264 \
  -pix_fmt yuv420p \
  -vf " \
    setpts=0.5*PTS[v]; \
    [v]pad=ceil(iw/2)*2:ceil(ih/2)*2" \
  -preset veryslow \
  -crf 10 \
  $1.mp4

ffmpeg \
  -i $1.nut \
  -vf " \
    setpts=0.5*PTS[v]; \
    [v]scale='min(1920,iw)':min'(1200,ih)': \
       force_original_aspect_ratio=decrease[v]; \
    [v]pad=ceil(iw/2)*2:ceil(ih/2)*2" \
  -preset veryslow \
  -crf 10 \
  -c:v libx264 \
  -pix_fmt yuv420p \
  $1_scaled.mp4

rm $t
