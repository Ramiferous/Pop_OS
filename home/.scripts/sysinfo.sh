#! /usr/bin/env bash

# Info
HOSTNAME=`uname -n`
#ROOT=`df -Ph | grep xvda1 | awk '{print $4}' | tr -d '\n'`
OS=`lsb_release -ds | tr -d '"'`
KERNEL=`uname -r`
CPU=`cat /proc/cpuinfo | grep 'model name' | uniq | awk '{print $6" "$8" "$9}'`
#SHELL=`$SHELL --version | grep release | awk '{print $1" "$2" "$4}' | sed 's/-release//'` # bash
SHELL=`$SHELL --version | awk '{print $1" "$2}'`
PKGS=`xbps-query -l | wc -l`
TERM=`echo $TERM`
FONT=`grep family $HOME/.config/alacritty/alacritty.toml | uniq | awk '{print $3" "$4}' | tr -d '"'`
SWAP=`free -m | tail -n 1 | awk '{print $3" MB";}'`
MEMORY1=`free -t -m | grep "Mem" | awk '{print $3" MB";}'`
MEMORY2=`free -t -m | grep "Mem" | awk '{print $2" MB";}'`
LOAD1=`cat /proc/loadavg | awk {'print $1'}`
LOAD5=`cat /proc/loadavg | awk {'print $2'}`
LOAD15=`cat /proc/loadavg | awk {'print $3'}`
BAT=`echo $(cat /sys/class/power_supply/BAT0/capacity)`
STATE=`cat /sys/class/power_supply/BAT0/status`
VOL=`wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print $2*100"%"}'`
BKLIGHT=`xbacklight -get | head -c 2`
TEMP=`acpi -t | awk '/0:/ {print $4}' | tr -d '.0'`

# Get uptime in seconds.
if [[ -r /proc/uptime ]]; then
    s=$(< /proc/uptime)
    s=${s/.*}
else
    boot=$(date -d"$(uptime -s)" +%s)
    now=$(date +%s)
    s=$((now - boot))
fi

d="$((s / 60 / 60 / 24)) days"
h="$((s / 60 / 60 % 24)) hours"
m="$((s / 60 % 60)) mins"

    # Remove plural if < 2.
((${d/ *} == 1)) && d=${d/s}
((${h/ *} == 1)) && h=${h/s}
((${m/ *} == 1)) && m=${m/s}

    # Hide empty fields.
((${d/ *} == 0)) && unset d
((${h/ *} == 0)) && unset h
((${m/ *} == 0)) && unset m

uptime=${d:+$d, }${h:+$h, }$m
uptime=${uptime%', '}
uptime=${uptime:-$s secs}

# Colour bar
COL=$(
        printf ''
    for i in 1 2 3 4 5 6; do
    printf '\033[9%sm▅▅' "$i"
    done
    printf '\033[0m\n'
  )

# Define Colours
if [ -x `command -v tput` ]; then
    bold=`tput bold`
    black=`tput setaf 0`
    red=`tput setaf 1`
    green=`tput setaf 2`
    yellow=`tput setaf 3`
    blue=`tput setaf 4`
    magenta=`tput setaf 5`
    cyan=`tput setaf 6`
    white=`tput setaf 7`
    reset=`tput sgr0`
fi

bl="${reset}${blue}"
yl="${reset}${yellow}"
rd="${reset}${red}"
mg="${reset}${magenta}"
bk="${reset}${black}"
gr="${reset}${green}"
cy="${reset}${cyan}"
wt="${reset}${white}"

echo $""
echo $"${bl}        _..._              "
echo $"${bl}      .'     '.      _      ${bk}➭  ${yl}OS${bk}................. " ${wt}$OS
echo $"${bl}     /    .----\   _/ \     ${bk}➭  ${yl}Kernel${bk}............. " ${wt}$KERNEL
echo $"${bl}   .-|   /:.   |  |   |     ${bk}➭  ${yl}Hostname${bk}........... " ${wt}$HOSTNAME
echo $"${bl}   |  \  |:.   /.-'-./      ${bk}➭  ${yl}Shell${bk}.............. " ${wt}$SHELL
echo $"${bl}   | .-'-;:__.'    =/       ${bk}➭  ${yl}Packages${bk}........... " ${wt}$PKGS
echo $"${bl}   .'=  *=|${rd}NASA${bl} _.='        ${bk}➭  ${yl}Term${bk}............... " ${wt}$TERM
echo $"${bl}  /  _ .  |    ;            ${bk}➭  ${yl}Font${bk}............... " ${wt}$FONT
echo $"${bl} ;-.-'|    \   |            ${bk}➭  ${yl}Uptime${bk}............. " ${wt}$uptime
echo $"${bl}/   | \    _\  _\           ${bk}➭  ${yl}CPU${bk}................ " ${wt}$CPU
echo $"${bl}\__/'._;.  ==' ==\          ${bk}➭  ${yl}Memory in use${bk}...... " ${wt}$MEMORY1
echo $"${bl}         \    \   |         ${bk}➭  ${yl}Temp${bk}............... " ${wt}$TEMP°c
echo $"${bl}         /    /   /         ${bk}➭  ${yl}Battery${bk}............ " ${wt}$BAT% $STATE
echo $"${bl}         /-._/-._/          ${bk}➭  ${yl}Volume${bk}............. " ${wt}$VOL
echo $"${bl}         \    \  \          ${bk}➭  ${yl}Backlight${bk}.......... " ${wt}$BKLIGHT%
echo $"${bl}          '-._/._/          ${bk}➭  ${yl}Colours${bk}............ " $COL
echo $"${reset}"
echo $""
