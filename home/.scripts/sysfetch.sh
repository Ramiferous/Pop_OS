#!/usr/bin/env bash

# color escapes
BLK="\e[30m"
RED="\e[31m"
GRN="\e[32m"
YLW="\e[33m"
BLU="\e[34m"
PUR="\e[35m"
CYN="\e[36m"
WHT="\e[37m"
GRY="\e[90;1m"
RST="\e[0m"
BLD="\033[1m"

# bars
#FULL=▓
#EMPTY=░
FULL=━
EMPTY=─
#FULL=─
#EMPTY=┄

name=$USER
host=$HOSTNAME
distro="$(uname -n) $(uname -s)"
kernel=$(uname -r)
pkgs=$(xbps-query -l | wc -l)
shell=$(basename $SHELL)
term=$(echo $TERM)
uptm=$(uptime | awk -F, '{sub(".*up ",x,$1);print $1}' | sed -e 's/^[ \t]*//;s/:/ hr /')
machine=$(cat /sys/devices/virtual/dmi/id/product_version | cut -d ' ' -f 1-2)

# Get wm
id="$(xprop -root -notype "_NET_SUPPORTING_WM_CHECK")"
id="${id##* }"
wm="$(xprop -id "$id" -notype -len "100" -f "_NET_WM_NAME" "8t")"
wm="${wm/*_NET_WM_NAME = }"
wm="${wm/\"}"
wm="${wm/\"*}"
wm="${wm,,}"

# Get init
init="$(readlink "/sbin/init")"
init="${init##*/}"
init="${init%%-*}"

# Get uptime in seconds.
if [[ -r /proc/uptime ]]; then
    s=$(< /proc/uptime)
    s=${s/.*}
else
    boot=$(date -d"$(uptime -s)" +%s)
    now=$(date +%s)
    s=$((now - boot))
fi

d="$((s / 60 / 60 / 24)) days"
h="$((s / 60 / 60 % 24)) hours"
m="$((s / 60 % 60)) mins"

    # Remove plural if < 2.
((${d/ *} == 1)) && d=${d/s}
((${h/ *} == 1)) && h=${h/s}
((${m/ *} == 1)) && m=${m/s}

    # Hide empty fields.
((${d/ *} == 0)) && unset d
((${h/ *} == 0)) && unset h
((${m/ *} == 0)) && unset m

uptime=${d:+$d, }${h:+$h, }$m
uptime=${uptime%', '}
uptime=${uptime:-$s secs}

# Get font
#confs=($HOME/.config/alacritty/alacritty.yml)
#term_font="$(awk -F ':|#' '/normal:/ {getline; print}' "${confs[0]}")"
#term_font="${term_font/*family:}"
#term_font="${term_font/$'\n'*}"
#term_font="${term_font/\#*}"

# Keep it simple...
term_font=$(grep family $HOME/.config/alacritty/alacritty.toml | uniq | awk '{print $3" "$4}' | tr -d '"')


# Cleanup first
clear

# find the center of the screen
COL=$(tput cols)
ROW=$(tput lines)
((PADY = ROW / 2 - 1 - 22 / 2))
((PADX = COL / 2 - 32 / 2))

for ((i = 0; i < PADX; ++i)); do
	PADC="$PADC "
done

for ((i = 0; i < PADY; ++i)); do
	PADR="$PADR\n"
done

# vertical padding
printf "%b" "$PADR"
printf "\n"

# Print Tree
BAR="████"
OUTT="$BLK$BAR$RED$BAR$GRN$BAR$YLW$BAR$BLU$BAR$PUR$BAR$CYN$BAR$WHT$BAR$RST"
printf "%s%b" "$PADC" "$OUTT"
printf "\n\n"

# greetings
printf "%s%b" "$PADC" "           hello $RED$BLD$name$RST\n"
printf "%s%b" "$PADC" "       welcome to the $GRN$BLD$host$RST\n"
printf "%s%b" "$PADC" "   i've been up for $CYN$BLD$uptime$RST\n\n"

# environment
printf "%s%b" "$PADC" "$YLW       machine $BLU⏹ $RST$machine\n"
printf "%s%b" "$PADC" "$YLW        distro $BLU⏹ $RST$distro\n"
printf "%s%b" "$PADC" "$YLW        kernel $BLU⏹ $RST$kernel\n"
printf "%s%b" "$PADC" "$YLW          init $BLU⏹ $RST$init\n"
printf "%s%b" "$PADC" "$YLW      packages $BLU⏹ $RST$pkgs\n"
printf "%s%b" "$PADC" "$YLW            wm $BLU⏹ $RST$wm\n"
printf "%s%b" "$PADC" "$YLW          term $BLU⏹ $RST$term\n"
printf "%s%b" "$PADC" "$YLW         shell $BLU⏹ $RST$shell\n"
printf "%s%b" "$PADC" "$YLW          font $BLU⏹ $RST$term_font\n"
printf " $RST\n"

# progress bar
draw() {
	perc=$1
	size=$2
	inc=$((perc * size / 100))
	out=
	color="$3"
	for v in $(seq 0 $((size - 1))); do
		test "$v" -le "$inc" &&
			out="${out}\e[1;${color}m${FULL}" ||
			out="${out}\e[0;37m${EMPTY}"
	done
	printf $out
}

# cpu
cpu=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}')
c_lvl=$(printf "%.0f" $cpu)
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " cpu" "$c_lvl%" $(draw $c_lvl 15 35)

# ram
ram=$(free | awk '/Mem:/ {print int($3/$2 * 100.0)}')
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " ram" "$ram%" $(draw $ram 15 35)

# battery
battery=$(echo $(cat /sys/class/power_supply/BAT0/capacity))
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " bat" "$battery%" $(draw $battery 15 35)

# temperature
temp=$(acpi -t | awk '/0:/ {print $4}' | tr -d '.0')
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " tmp" "$temp°c " $(draw $temp 15 35)

# backlight
backlight=$(xbacklight -get | head -c 2)
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " bkl" "$backlight%" $(draw $backlight 15 35)

# volume
volume=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{ print $2*100 }')
printf "%b" "$RST$PADC"
printf "   $PUR%-4s $WHT%-5s %-25s \n" " vol" "$volume%" $(draw $volume 15 35)

# hide the cursor and wait for user input
tput civis
read -n 1

# give the cursor back
tput cnorm
