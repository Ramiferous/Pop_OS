#!/bin/zsh

curl -F"file=@/home/david/pop-21.04.png" https://0x0.st >> /tmp/scrupload.txt

cat /tmp/scrupload.txt | xclip -selection clipboard && notify-send -u low "Screenshot Uploaded to $(xclip -o;echo)"
