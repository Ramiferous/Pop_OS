#!/bin/env bash
#
# fff & shfm open file in application based on file extension

case $(file -bi "$1") in
    image/*)
	sxiv "$1"
    #feh -g 700x393 --scale-down "$1"
    ;;

    audio/*)
	mpv --no-video "$1"
    ;;

    video/*)
	mpv "$1"
    ;;

    *.html)
    firefox "$1"
    ;;

    *.pdf)
	zathura "$1"
    ;;

    # all other filr types
    *)
	"${EDITOR:=nvim}" "$1"
	#xdg-open "$1"
    ;;
esac
