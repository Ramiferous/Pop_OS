#!/bin/sh
# require imagemagick, xrectsel
png=$(mktemp -p /tmp x11-snip.XXXXXXXX.png)
import -window root $png
convert $png -crop $(xrectsel '%wx%h+%x+%y') $png
printf '%s' "$png" | xsel -ib      # Optionaly copy image path to clipboard
notify-desktop -t 3000 -i $png 'SNIP!' 'Path copied to clipboard'
display $png                    # Optionaly display the snip
