# Fundamental-Round
# Author: Hund <http://ebupof.deviantart.com>
# License: GPL

gtk_color_scheme = "bg_color:#CFCFCF\nselected_bg_color:#808080\nbase_color:#F7F7F7" # Background, base.
gtk_color_scheme = "fg_color:#3C3C3C\nselected_fg_color:#F5F5F5\ntext_color:#3C3C3C" # Foreground, text.
gtk_color_scheme = "tooltip_bg_color:#DCDCDC\ntooltip_fg_color:#3C3C3C" # Tooltips.
gtk_color_scheme = "link_color:#5E8EC2" # Hyperlinks
gtk_color_scheme = "panel_bg:#3e3d3d" # Panel bg color. Not used here though.
gtk_color_scheme = "fm_color:#F7F9FA" # Color used in Nautilus and Thunar.
gtk_color_scheme = "frame_color:#CDCDCD\ninactive_frame_color:#CDCDCD" # Fix for Chrome

gtk-icon-sizes = "panel-menu=22,22:gtk-button=16,16"

include "panel.rc"

style "murrine-default"
{
	########
	# Style Properties
	########
	GtkButton      ::child-displacement-x = 1
	GtkButton      ::child-displacement-y = 1
	GtkButton      ::default-border       = { 0, 0, 0, 0 }
	GtkCheckButton ::indicator-size       = 12

	GtkPaned       ::handle-size          = 0

	GtkRange       ::trough-border        = 0
	GtkRange       ::slider-width         = 14
	GtkRange       ::stepper-size         = 14
	GtkRange          ::stepper_spacing              = 0
	GtkRange          ::trough-under-steppers        = 0

	GtkScale       ::slider-length        = 14
	GtkScale       ::slider-width	      = 13
	GtkScale       ::trough-side-details  = 1
	GtkScrollbar   ::min-slider-length    = 30

	GtkMenuBar     ::internal-padding     = 0
	GtkExpander    ::expander-size        = 16
	GtkToolbar     ::internal-padding     = 0
	GtkTreeView    ::expander-size        = 14
	GtkTreeView    ::vertical-separator   = 0

	GtkMenu        ::horizontal-padding   = 0
	GtkMenu        ::vertical-padding     = 0

	GtkWidget      ::focus-line-width     = 1

	GtkWidget         ::focus-padding                = 0

	GtkScrolledWindow ::scrollbar-spacing            = 0
	GtkScrolledWindow ::scrollbar-within-bevel       = 0

	# Glow the tasklist by changing the color, instead of overlaying it with a rectangle
	WnckTasklist   ::fade-overlay-rect    = 0

	# The following line hints to gecko (and possibly other appliations)
	# that the entry should be drawn transparently on the canvas.
	# Without this, gecko will fill in the background of the entry.
	GtkEntry          ::honors-transparent-bg-hint   = 1
	GtkEntry          ::state-hint                   = 0
	GtkEntry          ::progress-border              = { 2, 2, 2, 2 }

	GnomeHRef         ::link_color                   = @link_color		
	GtkHTML           ::link-color                   = @link_color
 	GtkIMHtmlr        ::hyperlink-color              = @link_color
	GtkIMHtml         ::hyperlink-color              = @link_color
	GtkWidget         ::link-color                   = @link_color
	GtkWidget         ::visited-link-color           = @text_color

	# Makes toolbars flat and unified
	#GtkToolbar	::shadow-type	      = GTK_SHADOW_NONE

	xthickness = 1
	ythickness = 1

	fg[NORMAL]        = @fg_color
	fg[PRELIGHT]      = @fg_color
	fg[ACTIVE]        = @fg_color
	fg[SELECTED]      = @selected_fg_color
	fg[INSENSITIVE]   = shade (0.65, @bg_color)

	bg[NORMAL]        = @bg_color
	bg[PRELIGHT]      = shade (1.02, @bg_color)
	bg[ACTIVE]        = shade (0.88, @bg_color)
	bg[SELECTED]	  = @selected_bg_color
	bg[INSENSITIVE]   = @bg_color

	base[NORMAL]      = @base_color
	base[PRELIGHT]    = shade (0.95, @bg_color)
	base[ACTIVE]      = shade (0.65, @bg_color)
	base[SELECTED]    = shade (1.05, @selected_bg_color)
	base[INSENSITIVE] = @bg_color

	text[NORMAL]      = @text_color
	text[PRELIGHT]    = @text_color
	text[ACTIVE]      = @selected_fg_color
	text[SELECTED]    = @selected_fg_color
	text[INSENSITIVE] = shade (0.65, @bg_color)

	engine "murrine" 
	{
		animation           = FALSE  # FALSE = disabled, TRUE = enabled
		contrast            = 0.8   # 0.8 for less contrast, more than 1.0 for more contrast on borders
		colorize_scrollbar  = FALSE  # FALSE = disabled, TRUE = enabled
		focus_color         = @selected_bg_color
		glazestyle          = 0     # 0 = flat, 1 = curved, 2 = concave, 3 = top-curved, 4 = beryl
		glow_shade          = 1.2  # sets glow amount for buttons or widgets
		glowstyle           = 0    # 0 = top, 1 = bottom, 2 = top and bottom, 3 = center (vertical), 4 = center (horizontal) 
		gradient_shades     = {1.1,1.02,1.02,0.94} # default: {1.1,1.0,1.0,1.1}
		gradient_colors	    = FALSE # {"#ffffff","#ffffff","#ffffff","#ffffff"}
		#prelight_shade	    = 1.0
		highlight_shade     = 1.0  # set highlight amount for buttons or widgets
		lightborder_shade   = 1.1   # sets lightborder amount for buttons or widgets
		lightborderstyle    = 0     # 0 = lightborder on top side, 1 = lightborder on all sides
		listviewheaderstyle = 1     # 0 = flat, 1 = glassy, 2 = raised
		listviewstyle       = 0     # 0 = nothing, 1 = dotted
		menubaritemstyle    = 1     # 0 = menuitem look, 1 = button look
		menubarstyle        = 2     # 0 = flat, 1 = glassy, 2 = gradient, 3 = striped
		menuitemstyle       = 1     # 0 = flat, 1 = glassy, 2 = striped
		menustyle           = 0     # 0 = no vertical menu stripe, 1 = display vertical menu stripe
		progressbarstyle    = 0     # 0 = no stripes, 1 = diagonal stripes, 2 = vertical stripes 
		reliefstyle	    = 1     # 0 = flat, 1 = inset, 2 = shadow, 3 = gradient on shadow, 4 = strong shadow
		rgba		    = FALSE # FALSE = disabled, TRUE = enabled
		roundness           = 2     # 0 = squared, 1 = old default, more will increase roundness
		scrollbarstyle      = 2     # 0 = nothing, 1 = circles, 2 = handles, 3 = diagonal stripes, 4 = diagonal stripes and handles, 5 = horizontal stripes, 6 = horizontal stripes and handles
		sliderstyle         = 1     # 0 = nothing added, 1 = handles
		stepperstyle        = 1     # 0 = standard, 1 = integrated stepper handles, 2 = squared steppers with a rounded slider
		toolbarstyle	    = 2     # 0 = flat, 1 = glassy, 2 = gradient
		arrowstyle	    = 1	    # 0 = old default, 1 = filled arrow
		border_shades	    = {1.0,1.0}	   # gradient on the border
		border_colors	    = FALSE # {"#ffffff","#ffffff"}
		comboboxstyle	    = 0	    # 0 = default, 1 = colorized GtkComboBox below the arrow
		#shadow_shades	    = {1.0,1.0}	   # gradient of shadows (only if reliefstyle = 3)
		#spinbuttonstyle	    = 0	   # 1 = separator on the GtkSpinButton
		textstyle	    = 0	   # 1 = draw an inset on the text (use carefully)
		#trough_shades	    = {1.0,1.0}	   # gradient on the trough of GtkScrollbar and GtkProgressBar
	}
}

style "nautilus-sidebar" {
GtkTreeView::even_row_color = @bg_color
} 

style "murrine-wide"
{
	xthickness = 1
	ythickness = 1
}

style "murrine-wider"
{
	xthickness = 3
	ythickness = 3
}

style "murrine-button" = "murrine-wider"
{
	bg[NORMAL]   = @bg_color
	bg[PRELIGHT] = shade (1.12, @bg_color)
	bg[ACTIVE]   = shade (0.9, @bg_color)
	bg[SELECTED] = @selected_bg_color # pre-selected buttons, keep same as focus colour
}

style "murrine-notebook" = "murrine-wide"
{
	bg[NORMAL] = shade (1.05, @bg_color)
	bg[ACTIVE] = shade (0.9, @bg_color)
}

style "murrine-tasklist" = "murrine-wide"
{
}

style "murrine-menu" = "murrine-wider"
{
	bg[NORMAL]   = shade (1.06, @bg_color)
	fg[PRELIGHT] = @selected_fg_color
}

style "murrine-menubar"
{
	bg[NORMAL]    = @bg_color
	bg[PRELIGHT]  = shade (1.0, @selected_bg_color)
	fg[PRELIGHT]  = @selected_fg_color
}

style "murrine-menu-item" = "murrine-wider"
{
	bg[PRELIGHT] = shade (1.0, @selected_bg_color)
	bg[SELECTED] = shade (1.0, @selected_bg_color)

	engine "murrine" 
	{
		gradient_shades = {1.18,1.05,1.05,0.94}
		contrast	= 0.3
	    	roundness = 0
	}
}

style "murrine-separator-menu-item"
{
}

style "murrine-treeview"
{
	base[SELECTED] 	= shade (1.0, @selected_bg_color)
	base[ACTIVE]	= "#8C8C8C"
	text[ACTIVE]	= @selected_fg_color

	engine "murrine"
	{
		gradient_shades = {1.1,1.05,1.05,0.95}
	}
}

style "murrine-treeview-header" = "murrine-default"
{
	engine "murrine"
	{
		gradient_shades     = {1.05,1.02,1.02,0.94}
		contrast	    = 0.8
	}
}

style "murrine-calendar"
{
	xthickness	= 0
	xthickness	= 0

	base[SELECTED] 	= shade (1.0, @selected_bg_color)
	base[ACTIVE]	= shade (0.9, @bg_color)
	text[ACTIVE]	= @fg_color

	engine "murrine"
	{
		gradient_shades     = {1.0,1.0,1.0,1.0}
	}
}

style "murrine-frame-title"
{
}

style "murrine-tooltips" = "murrine-wider"
{
	bg[NORMAL] = @tooltip_bg_color
	fg[NORMAL] = @tooltip_fg_color
}

style "murrine-progressbar"
{
	xthickness = 0
	ythickness = 0

	fg[PRELIGHT] = @selected_fg_color

	engine "murrine" 
	{
		contrast	= 1.0
		trough_shades	= {0.94,1.02}
		roundness         = 2
	}
}

style "murrine-statusbar"
{
}

style "murrine-comboboxentry"
{
	engine "murrine"
	{
		reliefstyle	= 1
	}
}

style "murrine-spinbutton"
{
}

style "murrine-scale"
{
	bg[PRELIGHT] = shade (1.0, @selected_bg_color)
	bg[SELECTED] = @selected_bg_color

	engine "murrine" 
	{
		contrast	= 0.8
	}
}

style "murrine-hscale"
{
}

style "murrine-vscale"
{
}

style "murrine-scrollbar"
{
	engine "murrine"
	{
		border_shades     = { .8, .6}
		border_colors     = { @bg_color, @bg_color }
		contrast          = 1.0
		glazestyle        = 1
		glow_shade        = 1.0
		glowstyle         = 0
		highlight_shade   = 1.00
		lightborder_shade = 1.2
		roundness         = 8
		trough_shades	    = {0.70,0.85}
	}
	
	bg[NORMAL]   = shade (1.05, @bg_color)
	bg[PRELIGHT] = shade (1.15, @bg_color)
	bg[ACTIVE]   = shade (1.0, @bg_color)
}

style "murrine-hscrollbar"
{
}

style "murrine-vscrollbar"
{
}

style "murrine-nautilus-location"
{
	bg[NORMAL] = @selected_bg_color
}

style "metacity-frame"
{
}

style "murrine-radiocheck"
{
	text[NORMAL]   = @selected_fg_color
	text[PRELIGHT] = @selected_fg_color
	bg[SELECTED] = shade (1.0, @selected_bg_color) # HACK: override button selection colour
}

style "murrine-entry" = "murrine-wider"
{
	engine "murrine"
	{
	}
}

style "murrine-expander" = "murrine-default"
{
	bg[PRELIGHT]	= shade (1.0, @selected_bg_color)

	engine "clearlooks"
	{
	}
}

style "radiocheck-menu"
{
	text[NORMAL]   = @fg_color
	text[PRELIGHT] = @selected_fg_color
               
        engine "murrine" {}
}

#########################################
# Matches
#########################################

# Theme default style is applied to every widget
class "GtkWidget"    					style "murrine-default"

# Increase the x/ythickness in some widgets
class "GtkToolbar"   					style "murrine-default" 
class "GtkRange"     					style "murrine-wide"
class "GtkFrame"     					style "murrine-wide"
class "GtkSeparator" 					style "murrine-wide"
class "GtkEntry"     					style "murrine-entry"

class "GtkSpinButton"  					style "murrine-spinbutton"
class "GtkScale"       					style "murrine-scale"
class "GtkVScale"      					style "murrine-vscale"
class "GtkHScale"      					style "murrine-hscale"
class "GtkScrollbar"   					style "murrine-scrollbar"
class "GtkVScrollbar"  					style "murrine-vscrollbar"
class "GtkHScrollbar"  					style "murrine-hscrollbar"

class "*GtkExpander*"  					style "murrine-expander"

class "GtkRadio*"                        		style:highest "murrine-radiocheck" # HACK: override button selection colour
class "GtkCheck*"                           		style:highest "murrine-radiocheck" # HACK: override button selection colour

# Use this to customize Metacity colours (causes issues with KDE/gtk-window-decorator)
class "MetaFrames" 					style "metacity-frame"
#class "GtkWindow"    			    		style "metacity-frame"

class "GtkCalendar"					style "murrine-calendar"

# General matching following, the order is choosen so that the right styles override each other
# eg. progressbar needs to be more important then the menu match.

# This is not perfect, it could be done better
# (That is modify *every* widget in the notebook, and change those back that
# we really don't want changed)
widget_class "NautilusNavigationWindow.GtkTable.NautilusHorizontalSplitter.NautilusSidePane.GtkNotebook.*Tree*" style "nautilus-sidebar"

widget_class "*<GtkNotebook>*<GtkEventBox>"     	style "murrine-notebook"
widget_class "*<GtkNotebook>*<GtkDrawingArea>"		style "murrine-notebook"
widget_class "*<GtkNotebook>*<GtkLayout>"       	style "murrine-notebook"
widget_class "*<GtkNotebook>*<GtkViewport>"		style "murrine-notebook"
widget_class "*<GtkNotebook>*<GtkScrolledWindow>"	style "murrine-notebook"

widget_class "*<GtkButton>"      			style "murrine-button"
widget_class "*<GtkNotebook>"    			style "murrine-notebook"
widget_class "*<GtkStatusbar>*"  			style "murrine-statusbar"

widget_class "*<GtkComboBoxEntry>*"			style "murrine-comboboxentry"
widget_class "*<GtkCombo>*"         			style "murrine-comboboxentry"

widget_class "*<GtkMenu>*"              		style "murrine-menu"
widget_class "*<GtkMenuItem>*"          		style "murrine-menu-item"
widget_class "*<GtkMenuBar>*"           		style "murrine-menubar"
widget_class "*<GtkSeparatorMenuItem>*" 		style "murrine-separator-menu-item"

widget_class "*.<GtkFrame>.<GtkLabel>" 			style "murrine-frame-title"
widget_class "*.<GtkTreeView>*"        			style "murrine-treeview"

widget_class "*<GtkProgress>"				style "murrine-progressbar"
widget_class "*<GtkProgressBar>"       			style "murrine-progressbar"

# Treeview header
widget_class "*.<GtkTreeView>.<GtkButton>" 		style "murrine-treeview-header"
widget_class "*.<GtkCTree>.<GtkButton>"    		style "murrine-treeview-header"
widget_class "*.<GtkList>.<GtkButton>"     		style "murrine-treeview-header"
widget_class "*.<GtkCList>.<GtkButton>"    		style "murrine-treeview-header"

# Workarounds for Evolution
widget_class "*.ETable.ECanvas"    			style "murrine-treeview-header"
widget_class "*.ETree.ECanvas"    			style "murrine-treeview-header"

class "GtkCheckMenuItem"                 style:highest "radiocheck-menu"

# The window of the tooltip is called "gtk-tooltip"
################################
# FIXME:
# This will not work if one embeds eg. a button into the tooltip.
# As far as I can tell right now we will need to rework the theme
# quite a bit to get this working correctly.
# (It will involve setting different priorities, etc.)
################################
widget "gtk-tooltip*" 					style "murrine-tooltips"

###################################################
# Special cases and work arounds
###################################################

# Special case the nautilus-extra-view-widget
# ToDo: A more generic approach for all applications that have a widget like this.
widget "*.nautilus-extra-view-widget" 			style : highest "murrine-nautilus-location"

# Work around for http://bugzilla.gnome.org/show_bug.cgi?id=382646
# Note that the work around assumes that the combobox is _not_ in
# appears-as-list mode.
# Similar hack also in the menuitem style.
# This style does not affect GtkComboBoxEntry, it does have an effect
# on comboboxes in appears-as-list mode though.
style "murrine-combobox-text-color-workaround"
{
	text[NORMAL]      = @fg_color
	text[PRELIGHT]    = @fg_color
	text[ACTIVE]      = @fg_color
	text[SELECTED]    = @selected_fg_color
	text[INSENSITIVE] = shade (0.65, @bg_color)
}
widget_class "*.<GtkComboBox>.<GtkCellView>"		style "murrine-combobox-text-color-workaround"

style "murrine-menuitem-text-is-fg-color-workaround"
{
	text[NORMAL]        = @fg_color
	text[PRELIGHT]      = @selected_fg_color
	text[ACTIVE]        = @fg_color
	text[SELECTED]      = @selected_fg_color
	text[INSENSITIVE]   = shade (0.65, @bg_color)
}

widget "*.gtk-combobox-popup-menu.*"   			style "murrine-menuitem-text-is-fg-color-workaround"

# Work around the usage of GtkLabel inside GtkListItems to display text.
# This breaks because the label is shown on a background that is based on the
# base color set.
style "murrine-fg-is-text-color-workaround"
{
	fg[NORMAL]      = @text_color
	fg[PRELIGHT]    = @text_color
	fg[ACTIVE]      = @selected_fg_color
	fg[SELECTED]    = @selected_fg_color
	fg[INSENSITIVE] = shade (0.65, @bg_color)
}

widget_class "*<GtkListItem>*" 				style "murrine-fg-is-text-color-workaround"

# The same problem also exists for GtkCList and GtkCTree
# Only match GtkCList and not the parent widgets, because that would also change the headers.
widget_class "*<GtkCList>" 				style "murrine-fg-is-text-color-workaround"

style "murrine-evo-new-button-workaround"
{

	engine "murrine"
	{
		toolbarstyle = 0
	}
}

widget_class "EShellWindow.GtkVBox.BonoboDock.BonoboDockBand.BonoboDockItem*" style "murrine-evo-new-button-workaround"

# Epiphany location entry list fix
style "ephy-location-fix"
{
	text[INSENSITIVE] = "#3574B2"
}
widget_class "*Ephy*Location*Entry*" style "ephy-location-fix"

include "nautilus.rc"
