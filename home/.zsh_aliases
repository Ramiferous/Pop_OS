#---------------------------------------------------------------
#                            Aliases
#---------------------------------------------------------------

# xbps
alias fpkg="fuzzypkg"
alias xupdate="doas xbps-install -Suv"
alias xinstall="doas xbps-install -S"
alias xsearch="xbps-query -R"
alias xremove="doas xbps-remove -RoO"
alias bootstrup="cd ~/xbpsrc/ && ./xbps-src bootstrap-update"
alias xupsrc="cd ~/xbpsrc/ && ./xbps-src update-sys"
alias zap="cd ~/xbpsrc/ && ./xbps-src zap"
alias binstrap="cd ~/xbpsrc/ && ./xbps-src binary-bootsrtap"

# random hlpful aliases
alias ..='cd ..'
alias mnt='doas mount -t msdos /dev/sda1 /mnt && cd /mnt'
alias umnt='cd && doas umount -t msdos /mnt'
alias ll='eza -l --group-directories-first'
alias la='eza -al --group-directories-first && echo "(`ls -a | wc -l`)"'
alias c="clear"
alias reload="source ~/.zshrc && clear && ufetch"
alias listen="mpv --no-video"
alias wttr="curl -s http://wttr.in/Melbourne | awk 'NR==2,NR==7'"
alias wttr2="curl v2.wttr.in/Melbourne | awk 'NR==39,NR==43'"
alias moon="~/.scripts/./moon.sh"
alias sdf="ssh pfr@tty.sdf.org"
alias fp="fzf --preview 'bat --style numbers,changes --color=always --theme=base16 --line-range=:500 {}'"
alias v="vim"
alias please='doas $(fc -ln -1)'
alias reboot="doas reboot"
alias goodbye="doas shutdown -h now"
alias et="sh ~/.scripts/empty-trash.sh"
alias h='fc -l -n -r 1 | sed -Ee "s/^[[:blank:]]+//" | uniq | fzf | tr -d \\n | xclip -selection c'
alias bat="bat --wrap character --theme=base16"
alias cat="bat -p --wrap character --theme=base16"
alias passgen="base64 /dev/urandom | head -c 30"
alias icat="kitty +kitten icat"

# nothing to see here
alias fap="cd ~/.fapdl/pron && nsxiv-thumb && cd"
alias fapdl=" cd ~/.fapdl/pron && yt-dlp -ciw -o '%(title)s.%(ext)s' --batch-file='~/.fapdl/batch-file.txt'"

# ytfzf
alias ytp="ytfzf -t"
alias ytd="cd ~/Videos/youtube && ytfzf -td"
alias ytpm="ytfzf -tm"
alias ytdm="cd ~/Music && ytfzf -tmd"

# git
alias status="git status"
alias add="git add ."
alias commit="git commit -m"
alias push="git push"
alias pull="git pull"

#---------------------------------------------------------------
#                           Functions
#---------------------------------------------------------------

# use a trash folder with rm
trash() {
    # Define the trash directory
    local trash_dir="$HOME/.local/share/Trash/"

    # Create trash directory if it doesn't exist
    [ ! -d "$trash_dir" ] && mkdir -p "$trash_dir"

    # Check if arguments are passed
    if [ "$#" -eq 0 ]; then
        echo "Usage: trash <file1> <file2> ..."
        return 1
    fi

    # Loop through each file argument
    for file in "$@"; do
        if [ -e "$file" ]; then
            # Generate a unique name to prevent overwriting in trash
            local timestamp
            timestamp=$(date +%Y%m%d%H%M%S)
            local trash_name="$trash_dir/$(basename "$file")_$timestamp"

            # Move the file to the trash directory
            command mv "$file" "$trash_name" && echo "Moved '$file' to trash." || echo "Failed to move '$file'."
        else
            echo "File '$file' does not exist."
        fi
    done
}
alias rm='trash'

# doas edit
doed() {
    doas vim -Z "$@"
}

# fastfetch
ff() {
    fastfetch \
    --logo-color-1 black \
    --logo-color-2 red \
    --color-keys green \
    --separator "..." \
    --colors-block-range-end 7
}

# shfm cd on exit
# !use 'command cat' to avoid conflicts with bat(1)
s () {
    shfm "$@"
    cd "$(command cat "${XDG_CACHE_HOME:=${HOME}/.cache}/shfm/exit")"
}

f () {
    fff "$@"
    cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}

# yt-dlp + ffplay
ytvideo() { yt-dlp -q --default-search "ytsearch" "$@" -o - | ffplay - -autoexit -loglevel quiet }
ytaudio() { yt-dlp -q --default-search "ytsearch" "$@" -o - | ffplay - -showmode 1 -x 600 -y 200 -autoexit -loglevel quiet }

# KNOWLEDGE BASE: ~/.kb
# use fzf with kb
kbv() {
    cd $HOME/.kb ; \
    fzf --preview 'bat {}' --bind 'enter:become(bat {})' ; \
    cd
}

kbe() {
    cd $HOME/.kb ; \
    fzf --preview 'bat {}' --bind 'enter:become(vim {})' ; \
    cd
}

kbd() {
    cd $HOME/.kb ; \
    rm $(fzf --preview 'bat {}') ; \
    cd
}

kbn() {
    cd /$HOME/.kb ; \
    nextvi ; \
    cd
  }

# mkdir and cd into it
#unalias md
md () { mkdir -p $1 && cd $1; }

# search & install packages using fzf (similar to fuzzypkg)
xs () {
    xpkg -a |
        fzf -m --preview 'xq {1}' \
            --preview-window=right:66%:wrap | \
        xargs -ro xi
}

# Swap 2 filenames around, if they exist
# usage: swap <file1> <file2>
swap () {
    local TMPFILE=tmp.$$
    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

# Creates an archive (*.tar.gz) from given directory.
mktar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Make your directories and files access rights sane.
sanitize() { chmod -R u=rwX,g=rX,o= "$@" ;}

# ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)    tar xjf $1      ;;
      *.tar.gz)     tar xzf $1      ;;
      *.tar.xz)     tar xf $1       ;;
      *.tar)        tar xf $1       ;;
      *.tar.zst)    uzstd $1        ;;
      *.bz2)        bunzip2 $1      ;;
      *.rar)        unrar x $1      ;;
      *.gz)         gunzip $1       ;;
      *.tbz2)       tar xjf $1      ;;
      *.tgz)        tar xzf $1      ;;
      *.zip)        unzip $1        ;;
      *.Z)          uncompress $1   ;;
      *.7z)         7z x $1         ;;
      *.deb)        ar x $1         ;;
      *.zst)        zstd -d $1      ;;
      *)      echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
