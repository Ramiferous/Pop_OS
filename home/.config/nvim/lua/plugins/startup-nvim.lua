local status, startup-nvim = pcall(require, "startup-nvim")
if not status then
  print("startup-nvim not installed")
  return
end

startup-nvim.setup({})
